package pl.edu.wat.wcy.jfk.i7b3s1.lab1.kennedy;



import pl.edu.wat.wcy.jfk.i7b3s1.lab1.kennedy.data.Data;
import pl.edu.wat.wcy.jfk.i7b3s1.lab1.kennedy.functions.Function;
import pl.edu.wat.wcy.jfk.i7b3s1.lab1.kennedy.functions.oneArgFunctions.IntFunction;
import pl.edu.wat.wcy.jfk.i7b3s1.lab1.kennedy.functions.oneArgFunctions.PushFunction;
import pl.edu.wat.wcy.jfk.i7b3s1.lab1.kennedy.functions.twoArgFunctions.MovFunction;
import pl.edu.wat.wcy.jfk.i7b3s1.lab1.kennedy.functions.twoArgFunctions.XorFunction;

import java.util.HashMap;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

 class Engine {
    private Map<String, Function> functionsMap = new HashMap<String, Function>();

    Engine() {
        Data data = new Data();
        IntFunction intFunction = new IntFunction(data);
        PushFunction pushFunction = new PushFunction(data);
        MovFunction movFunction = new MovFunction(data);
        XorFunction xorFunction = new XorFunction(data);
        functionsMap.put("int", intFunction);
        functionsMap.put("push", pushFunction);
        functionsMap.put("mov", movFunction);
        functionsMap.put("xor", xorFunction);
    }

     void execute(String command) {
        if(command.equals(""))
            return;

        String functionType = checkCommandAndReturnType(command);
        if (functionType == null) {
            System.out.println("Error");
            return;
        }
        Function function = functionsMap.get(functionType);
        String arguments = extractArguments(command, functionType);
        function.execute(arguments);
    }

    private String checkCommandAndReturnType(String command) {
        //Pattern pattern = Pattern.compile("((((mov)|(xor)) [^,]*,[^,]*)|(int \\s?0x80\\s*)|(push [^,]*))");
        Pattern pattern = Pattern.compile("\\s*((((mov)|(xor)) [^,]*,[^,]*)|(int \\s*0x80\\s*)|(push [^,]*))");

        //Sprawdzenie czy jest poprawnie calosc  command
        Matcher matcher = pattern.matcher(command);
        if (!matcher.matches()) {
            return null;
        }
        //Jaka jest/moze byc to funkcja
        pattern = Pattern.compile("(mov)|(xor)|(int)|(push)");
        matcher = pattern.matcher(command);
        matcher.find();
        return matcher.group();
    }

    private String extractArguments(String command, String functionType) {
        Pattern pattern = Pattern.compile("(?<=" + functionType + " ).*");
        Matcher matcher = pattern.matcher(command);
        matcher.find();
        return matcher.group();
    }
}
