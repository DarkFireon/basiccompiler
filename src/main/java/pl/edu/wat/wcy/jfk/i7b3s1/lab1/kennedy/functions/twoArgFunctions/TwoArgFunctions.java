package pl.edu.wat.wcy.jfk.i7b3s1.lab1.kennedy.functions.twoArgFunctions;


import pl.edu.wat.wcy.jfk.i7b3s1.lab1.kennedy.data.Data;
import pl.edu.wat.wcy.jfk.i7b3s1.lab1.kennedy.functions.Function;
import pl.edu.wat.wcy.jfk.i7b3s1.lab1.kennedy.parser.UnexpectedCharException;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public abstract class TwoArgFunctions extends Function {



    public TwoArgFunctions(Data data, String functionName) {
        super(data, functionName);
    }

    public void execute(String command) {

        if(checkForBlankBetweenNumbers(command)){
            System.out.println("Error");
            return;
        }
        command = removeBlankCharacters(command);
        String[] arguments = divide(command);

        if (!isItVariable(arguments[1])) {
            System.out.println("Error");
            return;
        }

        if(checkForSpecialXorSituation(arguments[0],arguments[1])){
            perform(arguments[1]);
        }

        String leftArgument = replaceVariablesForValues(arguments[0]);
        if(leftArgument.contains("x")){
            System.out.println("Error");
            return;
        }
        Integer value;
        //jezeli nie ma nie zainicjowanej wartosci
        if(!leftArgument.contains(",")) {

            try {
                value = evaluateArgument(leftArgument);
            } catch (UnexpectedCharException e) {
                System.out.println("Error");
                return;
            }
            perform(value, arguments[1]);
            return;
        }

        //jezeli w rownaniu jest null to sprawdz czy rownanie jest dobre jesli tak argumentem jest null jesli nie to error
        leftArgument=leftArgument.replace(",","x");
        try {
            value = evaluateArgument(leftArgument);
        } catch (UnexpectedCharException e) {
            System.out.println("Error");
            return;
        }
        perform(null, arguments[1]);
    }

    private boolean checkForSpecialXorSituation(String argument,String register) {
        //check for the same value in
        Integer value;
        if(this instanceof XorFunction){
            String arg= argument.replace(register,"0");
            arg = replaceVariablesForValues(arg);
            try {
                value = evaluateArgument(arg);
                if(value==0){
                    arg = argument.replace(register,"10");
                    arg = replaceVariablesForValues(arg);
                    value = evaluateArgument(arg);
                    if(value==10) {
                        return true;
                    }
                }
            } catch (UnexpectedCharException e) {
                return false;
            }
        }
        return false;
    }

    protected void perform(String register){};

    protected abstract void perform(Integer value, String argument);

    private String[] divide(String command) {
        String[] arguments = new String[2];
        Pattern pattern = Pattern.compile(".*(?=,)");
        Matcher matcher = pattern.matcher(command);
        matcher.find();
        arguments[0] = matcher.group();

        pattern = Pattern.compile("(?<=,).*");
        matcher = pattern.matcher(command);
        matcher.find();
        arguments[1] = matcher.group();

        return arguments;
    }
}
