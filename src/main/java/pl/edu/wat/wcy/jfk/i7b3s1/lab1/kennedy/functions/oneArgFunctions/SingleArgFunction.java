package pl.edu.wat.wcy.jfk.i7b3s1.lab1.kennedy.functions.oneArgFunctions;


import pl.edu.wat.wcy.jfk.i7b3s1.lab1.kennedy.data.Data;
import pl.edu.wat.wcy.jfk.i7b3s1.lab1.kennedy.functions.Function;

public abstract class SingleArgFunction extends Function {


    public SingleArgFunction(Data data, String functionName) {
        super(data, functionName);
    }

    public void execute(String argument) {
        if(checkForBlankBetweenNumbers(argument)){
            System.out.println("Error");
            return;
        }
        argument = removeBlankCharacters(argument);
        perform(argument);
    }

    protected abstract void perform(String argument);


}
