package pl.edu.wat.wcy.jfk.i7b3s1.lab1.kennedy.functions.oneArgFunctions;

import pl.edu.wat.wcy.jfk.i7b3s1.lab1.kennedy.data.Data;

public class IntFunction extends SingleArgFunction {


    public IntFunction(Data data) {
        super(data, "int");
    }

    protected void perform(String argument) {
        if (argument.equals("0x80")) {
            if (data.stackEmpty()) {
                System.out.println("???");
            } else {
                Integer integer = data.pop();
                if (integer == null) {
                    System.out.println("???");
                } else {
                    System.out.println(integer);
                }
            }
        }
    }


}
