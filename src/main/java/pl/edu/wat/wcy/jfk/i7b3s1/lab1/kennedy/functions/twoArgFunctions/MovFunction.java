package pl.edu.wat.wcy.jfk.i7b3s1.lab1.kennedy.functions.twoArgFunctions;


import pl.edu.wat.wcy.jfk.i7b3s1.lab1.kennedy.data.Data;

public class MovFunction extends TwoArgFunctions {

    public MovFunction(Data data) {
        super(data, "mov");
    }

    protected void perform(Integer value, String register) {
        data.setRegister(register, value);
    }
}