package  pl.edu.wat.wcy.jfk.i7b3s1.lab1.kennedy.functions;




import pl.edu.wat.wcy.jfk.i7b3s1.lab1.kennedy.data.Data;
import pl.edu.wat.wcy.jfk.i7b3s1.lab1.kennedy.parser.Parser;
import pl.edu.wat.wcy.jfk.i7b3s1.lab1.kennedy.parser.UnexpectedCharException;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public abstract class Function {

    protected Data data;
    protected final String functionName;
    public Function(Data data, String functionName) {
        this.data=data;
        this.functionName = functionName;
    }

    public abstract void execute(String argument);


    protected Integer evaluateArgument(String argument) throws UnexpectedCharException {
        Integer value;

        value = Parser.calculate(argument);

        return value;
    }

    protected boolean isItVariable(String input){
        Pattern pattern = Pattern.compile("[%]e[abcd]x");
        Matcher matcher = pattern.matcher(input);
        return matcher.matches();
    }

    protected String removeBlankCharacters(String stringToCheck){
        return stringToCheck.replaceAll("\\s+","");
    }

    protected String replaceVariablesForValues(String string){

        Pattern pattern = Pattern.compile("[%]e[abcd]x");
        Matcher matcher = pattern.matcher(string);
        while (matcher.find()) {
            if(data.getRegister(matcher.group())!=null) {
                string = string.replace(matcher.group(), String.valueOf(data.getRegister(matcher.group())));
            }else{
                string = string.replace(matcher.group(), ",");
            }
        }
        return string;
    }

    protected boolean validEquation(String string){
        Pattern pattern = Pattern.compile("[\\d[+][-][*][(][)]]*");
        Matcher matcher = pattern.matcher(string);
        if(matcher.matches()){
            return true;
        }
        return false;
    }

    protected  boolean checkForMoreThanOneSpace(String command){
        command=command.replaceFirst("\\s++$", "");
        return command.contains("  ");
    }


    protected boolean checkForBlankBetweenNumbers(String string){
        Pattern pattern = Pattern.compile("\\d\\s+\\d");
        Matcher matcher = pattern.matcher(string);
        if(matcher.find()){
            return true;
        }
        return false;


    }


}
