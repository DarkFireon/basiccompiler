package pl.edu.wat.wcy.jfk.i7b3s1.lab1.kennedy;
import java.util.Scanner;

public class Main {


    public static void main(String[] args) {

        Scanner sc = new Scanner(System.in);
        Engine engine = new Engine();
        String command;


        do {
            command = sc.nextLine();
            engine.execute(command);
        } while (sc.hasNext());
    }
}
