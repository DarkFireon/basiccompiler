package pl.edu.wat.wcy.jfk.i7b3s1.lab1.kennedy.data;

import java.util.HashMap;
import java.util.Map;
import java.util.Stack;

public class Data {

    private Map<String, Integer> registers = new HashMap<String,Integer>();
    private Stack<Integer> stack = new Stack<Integer>();

    public Data(){
        Integer eax = null;
        Integer ebx = null;
        Integer ecx =null;
        Integer edx = null;
        registers.put("%eax",eax);
        registers.put("%ebx",ebx);
        registers.put("%ecx",ecx);
        registers.put("%edx",edx);
    }

    public Integer getRegister(String register){
       return registers.get(register);
    }

    public void setRegister(String register,Integer value){
        if(registers.containsKey(register)) {
            registers.put(register, value);
        }else{
            System.out.println("Error");
        }
    }
    public void showRegisters(){
        for (Map.Entry<String, Integer> entry : registers.entrySet()) {
            System.out.println("Key = " + entry.getKey() + ", Value = " + entry.getValue());
        }
    }
    public void push(Integer value) {
        stack.push(value);
    }

    public Integer pop() {
       return  (Integer) stack.pop();
    }

    public boolean stackEmpty() {
        return stack.empty();
    }
}
