package pl.edu.wat.wcy.jfk.i7b3s1.lab1.kennedy.functions.twoArgFunctions;


import pl.edu.wat.wcy.jfk.i7b3s1.lab1.kennedy.data.Data;

public class XorFunction extends TwoArgFunctions {

    public XorFunction(Data data) {
        super(data, "xor");
    }

    protected void perform(Integer value, String register) {

        if(value == null|| data.getRegister(register)==null){
            data.setRegister(register, null);
            return;
        }
        Integer xorValue = xorResult(value, data.getRegister(register));
        data.setRegister(register, xorValue);
    }

    protected void perform(String register) {
        data.setRegister(register, 0);
    }

    int xorResult(Integer firstArg, Integer secondArg) {
        return (firstArg ^ secondArg);
    }


}
