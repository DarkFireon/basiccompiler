package pl.edu.wat.wcy.jfk.i7b3s1.lab1.kennedy.parser;


 public  class Parser {
    private static  int pos = -1, ch;
    private static String str;
    private static void nextChar() {
        ch = (++pos < str.length()) ? str.charAt(pos) : -1;
    }

    private static boolean checkFor(int charToEat) {
        if (ch == charToEat) {
            nextChar();
            return true;
        }
        return false;
    }
    public static Integer calculate(String stringToParse) throws UnexpectedCharException {
        pos = -1;
        str = stringToParse;
        nextChar();
        Integer x = parseExpression();
        if (pos < str.length()) throw new UnexpectedCharException("Unexpected: " + (char) ch);
        return x;
    }
    private static Integer parseExpression() throws UnexpectedCharException {
        Integer x = parseTerm();
        for (; ; ) {
            if (checkFor('+')) x += parseTerm(); // addition
            else if (checkFor('-')) x -= parseTerm(); // subtraction
            else return x;
        }
    }
    private static Integer parseTerm() throws UnexpectedCharException {
        Integer x = parseFactor();
        for (; ; ) {
            if (checkFor('*')) x *= parseFactor(); // multiplication
            else return x;
        }
    }
    private static Integer parseFactor() throws UnexpectedCharException {
        if (checkFor('-')) return -parseFactor(); // unary minus
        if (checkFor('+')) return parseFactor(); // unary plus
        Integer x = null;
        int startPos = pos;
        if (checkFor('(')) { // parentheses
            x = parseExpression();
            if(!checkFor(')')){
                throw new UnexpectedCharException("expected  )");
            }
        } else if (ch >= '0' && ch <= '9') { // numbers
            while (ch >= '0' && ch <= '9') nextChar();
            x = Integer.parseInt(str.substring(startPos, pos));
        } else if (ch=='x') { // numbers
            nextChar();
            x = 1;
        } else {
            throw new UnexpectedCharException("Unexpected: " + (char) ch);
        }
        return x;
    }





}
