package  pl.edu.wat.wcy.jfk.i7b3s1.lab1.kennedy.parser;

 public class UnexpectedCharException extends Exception {
    UnexpectedCharException(String string){
        super(string);
    }
}
