Kompilator na wzór języka asemblera.
-Dostępne są cztery rejestry %eax, %ebx, %ecx i %edx oraz stos.
-Rejestry na poczatku maja nie przypisana wartosc i ich wyswietlenie zwraca "???"
-pusty stos również zwraca ???
-dostepne sa nastepujace funkcje : MOV, XOR, PUSH, INT
-"int 0x80" zdejmuje i wyswielta wartosc ze stosu
-"mov $1,$2" przesyła wartosc z parametu $1 do $2.
~parametrem $1 moze byc wartosc arytmetyczna uwzgledniajaca dodawanie, odejmowanie, mnozenie np -2*(3+3)
~wartosc arytmetyczna moze zawierac rejestr np mov (%ecx+3)*2,%eax
~parametr $2 musi zawierac wskaznik na rejestr czyli np %ecx
-"xor $1,$2" wykonuje działanie na dwoch parametrach i wstawia wartosc do wskaznika w parametrze 2
~sposob działania funkcji xor jest analogiczny co do mov
~xor z niezaincjowana wartoscia zwroci ???
~xor z dwoma takimi samymi wartosciami zwroci zawsze 0 nawet z niezainicjowanymi
- "push $1" wstawia parametr $1 na stos np "push $eax+2"
-białe znaki są ignorowane
-wielkosc liter ma znaczenie