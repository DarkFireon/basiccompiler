package pl.edu.wat.wcy.jfk.i7b3s1.lab1.kennedy.functions.oneArgFunctions;


import pl.edu.wat.wcy.jfk.i7b3s1.lab1.kennedy.data.Data;
import pl.edu.wat.wcy.jfk.i7b3s1.lab1.kennedy.parser.UnexpectedCharException;

public class PushFunction extends SingleArgFunction {


    public PushFunction(Data data) {
        super(data, "push");
    }


    protected void perform(String argument) {
        argument = replaceVariablesForValues(argument);
        if(argument.contains("x")){
            System.out.println("Error");
            return;
        }
        Integer value;
        if(!argument.contains(",")) {

            try {
                value = evaluateArgument(argument);
            } catch (UnexpectedCharException e) {
                System.out.println("Error");
                return;
            }
            data.push(value);
            return;
        }

        //jezeli w rownaniu jest null(",") to sprawdz czy rownanie jest dobre jesli tak argumentem jest null jesli nie to error
        argument=argument.replace(",","x");
        try {
            value = evaluateArgument(argument);
        } catch (UnexpectedCharException e) {
            System.out.println("Error");
            return;
        }
        data.push(null);
    }
}
